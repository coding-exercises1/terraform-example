terraform {
  required_providers {
   aws = {
    source = "hashicorp/aws"
    version = "~> 3.5.0"
  }
 }
}

provider "aws" {
 region     = "us-east-2"
  access_key = "123standin"
  secret_key = "456standin"
}

resource "aws_budgets_budget" "example" {
  name            = "monthly-budget"
  budget_type     = "COST"
  limit_amount    = "650.0"
  limit_unit      = "USD"
  time_unit       = "MONTHLY"
  time_period_start = "2024-02-25"
  time_period_end   = "2024-03-25" 
}
